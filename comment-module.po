# Adam Rambousek <xrambous@fi.muni.cz>, 2005, 2006.
# Testing
msgid ""
msgstr ""
"Project-Id-Version: comment-module\n"
"POT-Creation-Date: 2006-05-01 13:01+0200\n"
"PO-Revision-Date: 2006-05-02 21:07GMT+1\n"
"Last-Translator: Adam Rambousek <xrambous@fi.muni.cz>\n"
"Language-Team: Czech\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: KBabel 1.10.2\n"

#: modules/comment.module:73
msgid "The comment module creates a discussion board for each post. Users can post comments to discuss a forum topic, weblog post, story, collaborative book page, etc. The ability to comment is an important part of involving members in a community dialogue."
msgstr "Modul comment umožňuje vytvoření diskusního panelu ke každému uzlu. Uživatelé mohou přidávat komentáře k diskusnímu fóru, blogu, článku, stránce knihy atd. Možnost komentářů je důležitou součástí zapojení lidí do komunity."

#: modules/comment.module:74
msgid "An administrator can give comment permissions to user groups, and users can (optionally) edit their last comment, assuming no others have been posted since.  Attached to each comment board is a control panel for customizing the way that comments are displayed. Users can control the chronological ordering of posts (newest or oldest first) and the number of posts to display on each page.  Comments behave like other user submissions. Filters, smileys and HTML that work in nodes will also work with comments. The comment module provides specific features to inform site members when new comments have been posted."
msgstr "Administrátor může udělit oprávnění k posílání komentářů jen některé skupině uživatelů a uživatelé mohou případně upravovat své poslední příspěvky, pokud na jejich komentář ještě nikdo neodpověděl. Ke komentářům je připojen ovládací panel, kde mohou uživatelé upravit způsob zobrazení komentářů. Uživatelé mohou měnit chronologické pořadí komentářů (od nejstarších nebo nejnovějších) a počet komentářů na stránce. Komentáře se chovají stejně jako jiné příspěvky v Drupalu. Filtry, smajlíky a HTML, které funguje v uzlech, bude fungovat stejně i v komentářích. Modul poskytuje několik vlastností, které informují uživatele o nově přidaných komentářích. Na webech, kde uživatelé posílají mnoho komentářů, může administrátor zapnout moderování komentářů uživateli."

#: modules/comment.module:75
msgid ""
"<p>You can</p>\n"
"<ul>\n"
"<li>control access for various comment module functions through access permissions <a href=\"%admin-access\">administer &gt;&gt; access control</a>.</li>\n"
"<li>administer comments <a href=\"%admin-comment-configure\"> administer &gt;&gt; comments &gt;&gt; configure</a>.</li>\n"
"</ul>\n"
msgstr ""
"<p>Můžete</p>\n"
"<ul>\n"
"<li>řídit přístup k různým funkcí modulu comment na stránce <a href=\"%admin-access\">administrace &gt;&gt; řízení přístupu</a>.</li>\n"
"<li>administrovat komentář <a href=\"%admin-comment-configure\"> administrace &gt;&gt; komentáře &gt;&gt; nastavení</a>.</li>\n"
"</ul>\n"

#: modules/comment.module:81
msgid "For more information please read the configuration and customization handbook <a href=\"%comment\">Comment page</a>."
msgstr "Více informací najdete v příručce pro nastavení a úpravy na <a href=\"%comment\">stránce Comment</a>."

#: modules/comment.module:84
msgid "Allows users to comment on and discuss published content."
msgstr "Umožňuje uživatelům komentovat obsah stránek."

#: modules/comment.module:87
msgid "<p>Below is a list of the latest comments posted to your site. Click on a subject to see the comment, the author's name to edit the author's user information , \"edit\" to modify the text, and \"delete\" to remove their submission.</p>"
msgstr "<p>Níže je seznam nejnovějších přidaných komentářů. Po kliknutí na předmět se zobrazí celý komentář, kliknutím na jméno autora můžete upravit informace uživatele, \"upravit\" upraví text a \"smazat\" odstraní příspěvek.</p>"

#: modules/comment.module:89
msgid "<p>Below is a list of the comments posted to your site that need approval. To approve a comment, click on \"edit\" and then change its \"moderation status\" to Approved. Click on a subject to see the comment, the author's name to edit the author's user information, \"edit\" to modify the text, and \"delete\" to remove their submission.</p>"
msgstr "<p>Níže je seznam komentářů, které čekají na schávlení. Pro schválení komentáře klikněte na \"upravit\" a poté změňte stav na Schváleno. Po kliknutí na předmět se zobrazí celý komentář, kliknutím na jméno autora můžete upravit informace uživatele, \"upravit\" upraví text a \"smazat\" odstraní příspěvek.</p>"

#: modules/comment.module:92
msgid "<p>Comments can be attached to any node, and their settings are below. The display comes in two types: a \"flat list\" where everything is flush to the left side, and comments come in chronological order, and a \"threaded list\" where replies to other comments are placed immediately below and slightly indented, forming an outline. They also come in two styles: \"expanded\", where you see both the title and the contents, and \"collapsed\" where you only see the title. Preview comment forces a user to look at their comment by clicking on a \"Preview\" button before they can actually add the comment.</p>"
msgstr "<p>Komentáře lze připojit k jakémukoliv uzlu a zde je jejich nastavení. Komentáře je možno zobrazit dvěma způsoby: \"bez vláken\", kde jsou všechny komentáře zarovnány vlevo a seřazeny podle času přidání, nebo \"vlákna\", kde se odpovědi na komentáře umisťují pod sebe a odsazují. Také je možno vybrat ze dvou stylů: \"rozbaleno\", kdy vidíte titulek i obsah, a \"sbaleno\", kdy vidíte jen titulek. Pokud vyberete povinné zobrazení komentáře, musí uživatel nejprve kliknout na tlačítko \"Náhled\" a podívat se na komentář, než bude přidán.</p>"

#: modules/comment.module:104
msgid "comments"
msgstr "komentáře"

#: modules/comment.module:114
msgid "published comments"
msgstr "zveřejněné komentáře"

#: modules/comment.module:116
msgid "approval queue"
msgstr "fronta ke schválení"

#: modules/comment.module:124
msgid "delete comment"
msgstr "smazat komentář"

#: modules/comment.module:128
msgid "edit comment"
msgstr "upravit komentář"

#: modules/comment.module:135
msgid "reply to comment"
msgstr "odpovědět na komentář"

#: modules/comment.module:163;167
msgid "Recent comments"
msgstr "Poslední komentáře"

#: modules/comment.module:198
msgid "Jump to the first comment of this posting."
msgstr "Skočí na první komentář tohoto příspěvku."

#: modules/comment.module:201
msgid "Jump to the first new comment of this posting."
msgstr "Skočí na první nový komentář tohoto příspěvku."

#: modules/comment.module:207;223
msgid "add new comment"
msgstr "Přidat komentář"

#: modules/comment.module:207
msgid "Add a new comment to this page."
msgstr "Přidat komentář k této stránce."

#: modules/comment.module:223
msgid "Share your thoughts and opinions related to this posting."
msgstr "Podělte se o své myšlenky a názory k tomuto příspěvku."

#: modules/comment.module:243
msgid "Default comment setting"
msgstr "Výchozí nastavení komentářů"

#: modules/comment.module:243;259
msgid "Read only"
msgstr "Jen čtení"

#: modules/comment.module:243;259
msgid "Read/Write"
msgstr "Čtení/Psaní"

#: modules/comment.module:243
msgid "Users with the <em>administer comments</em> permission will be able to override this setting."
msgstr "Uživatelé s právem <em>administrace komentářů</em> mohou toto nastavení změnit."

#: modules/comment.module:250;324
msgid "Comment settings"
msgstr "Nastavení komentářů"

#: modules/comment.module:329
msgid "Signature"
msgstr "Podpis"

#: modules/comment.module:331
msgid "Your signature will be publicly displayed at the end of your comments."
msgstr "Váš podpis se připojí na konec vašich komentářů."

#: modules/comment.module:347
msgid "Viewing options"
msgstr "Možnosti zobrazení"

#: modules/comment.module:354
msgid "Default display mode"
msgstr "Standardní režim zobrazení"

#: modules/comment.module:357
msgid "The default view for comments. Expanded views display the body of the comment. Threaded views keep replies together."
msgstr "Standardní zobrazení komentářů. Při rozšířeném zobrazení je vidět tělo komentáře. Při vláknovém zobrazení se odpovědi drží u sebe."

#: modules/comment.module:362
msgid "Default display order"
msgstr "Standardní pořadí"

#: modules/comment.module:365
msgid "The default sorting for new users and anonymous users while viewing comments. These users may change their view using the comment control panel. For registered users, this change is remembered as a persistent user preference."
msgstr "Standardní řazení pro nové uživatele a anonymní uživatele. Uživatelé si mohou změnit zobrazení na ovládací panelu. Pro registrované uživatele se toto nastavení uloží."

#: modules/comment.module:370
msgid "Default comments per page"
msgstr "Standardní počet komentářů na stránku"

#: modules/comment.module:373
msgid "Default number of comments for each page: more comments are distributed in several pages."
msgstr "Standardní počet komentářů na každou stránku; více komentářů se rozdělí na několik stránek."

#: modules/comment.module:378
msgid "Comment controls"
msgstr "Ovládání komentářů"

#: modules/comment.module:381
msgid "Display above the comments"
msgstr "Zobrazit nad komentáři"

#: modules/comment.module:382
msgid "Display below the comments"
msgstr "Zobrazit pod komentáři"

#: modules/comment.module:383
msgid "Display above and below the comments"
msgstr "Zobrazit nad i pod komentáři"

#: modules/comment.module:384
msgid "Do not display"
msgstr "Nezobrazovat"

#: modules/comment.module:385
msgid "Position of the comment controls box.  The comment controls let the user change the default display mode and display order of comments."
msgstr "Umístění ovládání komentářů. Uživatel může změnit standarní režim zobrazení a pořadí komentářů."

#: modules/comment.module:390
msgid "Posting settings"
msgstr "Nastavení posílání"

#: modules/comment.module:397
msgid "Anonymous commenting"
msgstr "Anonymní komentáře"

#: modules/comment.module:400
msgid "Anonymous posters may not enter their contact information"
msgstr "Anonymní uživatelé nesmí zadat kontaktní informace"

#: modules/comment.module:401
msgid "Anonymous posters may leave their contact information"
msgstr "Anonymní uživatelé mohou zadat kontaktní informace"

#: modules/comment.module:402
msgid "Anonymous posters must leave their contact information"
msgstr "Anonymní uživatelé musí zadat kontaktní informace"

#: modules/comment.module:403
msgid "This option is enabled when anonymous users have permission to post comments on the <a href=\"%url\">permissions page</a>."
msgstr "Toto nastavení má smysl jen, když povolíte anonymním uživatelům psát komentáře. Podívejte se na <a href=\"%url\">stránku oprávnění</a>."

#: modules/comment.module:411
msgid "Comment subject field"
msgstr "Předmět komentáře"

#: modules/comment.module:414
msgid "Can users provide a unique subject for their comments?"
msgstr "Mohou uživatelé uvádět nové téma v komentářích?"

#: modules/comment.module:419;482;1332;1338;1342;1363
msgid "Preview comment"
msgstr "Zobrazit komentář"

#: modules/comment.module:426
msgid "Location of comment submission form"
msgstr "Umístění formuláře k odeslání komentáře"

#: modules/comment.module:428
msgid "Display on separate page"
msgstr "Zobrazit na zvláštní stránce"

#: modules/comment.module:428
msgid "Display below post or comments"
msgstr "Zobrazit pod příspěvkem nebo komentářem"

#: modules/comment.module:487;523
msgid "You are not authorized to post comments."
msgstr "Nemáte povolení posílat komentáře."

#: modules/comment.module:498;506
msgid "The comment you are replying to does not exist."
msgstr "Komentář, na které odpovídáte, neexistuje."

#: modules/comment.module:516
msgid "This discussion is closed: you can't post new comments."
msgstr "Tato diskuse je zavřená: nemůžete psát nové komentáře."

#: modules/comment.module:520
msgid "Reply"
msgstr "Odpovědět"

#: modules/comment.module:529
msgid "You are not authorized to view comments."
msgstr "Nemáte povolení prohlížet komentáře."

#: modules/comment.module:554
msgid "Comment: duplicate %subject."
msgstr "Komentář: duplikace %subject."

#: modules/comment.module:568;1016
msgid "Comment: updated %subject."
msgstr "Komentář: aktualizován %subject."

#: modules/comment.module:641
msgid "Comment: added %subject."
msgstr "Komentář: přidán %subject."

#: modules/comment.module:650
msgid "Your comment has been queued for moderation by site administrators and will be published after approval."
msgstr "Váš komentář byl zařazen do fronty ke schválení administrátory webu a bude zveřejněn po schválení."

#: modules/comment.module:659
msgid "Comment: unauthorized comment submitted or comment submitted to a closed node %subject."
msgstr "Komentář: zaslán neautorizovaný komentář nebo komentář k uzavřenému uzlu %subject."

#: modules/comment.module:673
msgid "parent"
msgstr "nadřazený"

#: modules/comment.module:680;686
msgid "reply"
msgstr "odpovědět"

#: modules/comment.module:865
msgid "Post new comment"
msgstr "Poslat nový komentář"

#: modules/comment.module:884
msgid "The comment and all its replies have been deleted."
msgstr "Komentář a všechny odpovědi byly smazány."

#: modules/comment.module:899
msgid "Are you sure you want to delete the comment %title?"
msgstr "Opravdu chcete smazat komentář %title?"

#: modules/comment.module:901
msgid "Any replies to this comment will be lost. This action cannot be undone."
msgstr "Budou odstraněny také reakce na tento komentář. Tuto akci nelze vrátit zpět."

#: modules/comment.module:906
msgid "The comment no longer exists."
msgstr "Komentář již neexistuje."

#: modules/comment.module:919;931
msgid "Publish the selected comments"
msgstr "Zveřejnit vybrané komentáře"

#: modules/comment.module:920;926;933
msgid "Delete the selected comments"
msgstr "Smazat vybrané komentáře"

#: modules/comment.module:925;932
msgid "Unpublish the selected comments"
msgstr "Nezveřejnit vybrané komentáře"

#: modules/comment.module:993
msgid "Please select one or more comments to perform the update on."
msgstr "Zvolte komentáře, které chcete aktualizovat."

#: modules/comment.module:1039
msgid "No comments available."
msgstr "Bez komentářů."

#: modules/comment.module:1068
msgid "Are you sure you want to delete these comments and all their children?"
msgstr "Určitě chcete smazat tyto komentáře a jejich potomky?"

#: modules/comment.module:1070
msgid "Delete comments"
msgstr "Smazat komentáře"

#: modules/comment.module:1084
msgid "The comments have been deleted."
msgstr "Komentáře byly smazány."

#: modules/comment.module:1161
msgid "You have to specify a valid author."
msgstr "Musíte zadat platného autora."

#: modules/comment.module:1166
msgid "The body of your comment is empty."
msgstr "Nenapsal jste komentář."

#: modules/comment.module:1176
msgid "The name you used belongs to a registered user."
msgstr "Jméno, které jste zadal, patří registrovanému uživateli."

#: modules/comment.module:1181
msgid "You have to leave your name."
msgstr "Musíte zadat své jméno."

#: modules/comment.module:1186
msgid "The e-mail address you specified is not valid."
msgstr "Zadal jste neplatnou e-mailovou adresu."

#: modules/comment.module:1190
msgid "You have to leave an e-mail address."
msgstr "Musíte zadat e-mailovou adresu."

#: modules/comment.module:1195
msgid "The URL of your homepage is not valid.  Remember that it must be fully qualified, i.e. of the form <code>http://example.com/directory</code>."
msgstr "URL stránky není platné. Adresa musí být kompletní, tj. například <code>http://example.com/directory</code>."

#: modules/comment.module:1242
msgid "Administration"
msgstr "Administrace"

#: modules/comment.module:1285;1310;1317
msgid "Homepage"
msgstr "Domovská stránka"

#: modules/comment.module:1294
msgid "Not published"
msgstr "Nevydáno"

#: modules/comment.module:1324
msgid "Comment"
msgstr "Komentář"

#: modules/comment.module:1338;1339
msgid "Post comment"
msgstr "Odeslat komentář"

#: modules/comment.module:1497
msgid "%a comments per page"
msgstr "%a komentářů na stránku"

#: modules/comment.module:1506
msgid "Save settings"
msgstr "Uložit nastavení"

#: modules/comment.module:1517
msgid "Select your preferred way to display the comments and click \"Save settings\" to activate your changes."
msgstr "Vyberte si, jak chcete zobrazovat komentáře a klikněte na \"Uložit změny\"."

#: modules/comment.module:1518
msgid "Comment viewing options"
msgstr "Volby prohlížení komentářů"

#: modules/comment.module:1541
msgid "by %a on %b"
msgstr "napsal %a, %b"

#: modules/comment.module:1551
msgid "by"
msgstr "napsal"

#: modules/comment.module:1589
msgid "you can't post comments"
msgstr "nemůžete psát komentáře"

#: modules/comment.module:1601
msgid "<a href=\"%login\">login</a> or <a href=\"%register\">register</a> to post comments"
msgstr "<a href=\"%login\">Přihlašte se</a> nebo <a href=\"%register\">zaregistrujte</a> , abyste mohl psát komentáře"

#: modules/comment.module:1604
msgid "<a href=\"%login\">login</a> to post comments"
msgstr "<a href=\"%login\">Přihlašte se</a>, abyste mohl psát komentáře"

#: modules/comment.module:1612
msgid "Comment: deleted %subject."
msgstr "Komentář: smazán %subject."

#: modules/comment.module:1632
msgid "Flat list - collapsed"
msgstr "Bez vláken - sbaleno"

#: modules/comment.module:1633
msgid "Flat list - expanded"
msgstr "Bez vláken - rozbaleno"

#: modules/comment.module:1634
msgid "Threaded list - collapsed"
msgstr "Vlákna - sbalené"

#: modules/comment.module:1635
msgid "Threaded list - expanded"
msgstr "Vlákna - rozbaleno"

#: modules/comment.module:201
msgid "1 new comment"
msgid_plural "%count new comments"
msgstr[0] "%count nový komentář"
msgstr[1] "%count nové komentáře"
msgstr[2] "%count nových komentářů"

#: modules/comment.module:153
msgid "access comments"
msgstr "přístup ke komentářům"

#: modules/comment.module:153
msgid "post comments"
msgstr "psát komentáře"

#: modules/comment.module:153
msgid "administer comments"
msgstr "administrace komentářů"

#: modules/comment.module:153
msgid "post comments without approval"
msgstr "psát komentáře bez schválení"

#: modules/comment.module:0
msgid "comment"
msgstr "komentář"

